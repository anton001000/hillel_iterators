

class frange:
    def __init__(self, first_args, second_arg=None, third_arg=None):
        self.first_args = first_args
        self.second_arg = second_arg
        self.third_arg = third_arg
        self.args_list = [first_args, second_arg, third_arg]
        self._pointer = 0
        self._is_float = self._is_float_function()
        self._count_args = self._count_elements()
        self._execute = self._execute()

    def __iter__(self):
        return self

    def __next__(self):
        if self._pointer == len(self._execute):
            raise StopIteration
        result = self._execute[self._pointer]
        self._pointer += 1
        return result

    def _execute(self):
        if self._is_float:
            list_res = []
            list_res.append(self.first_args)
            num = self.first_args
            while num <= self.second_arg:
                num += self.third_arg
                list_res.append(num)
            return list_res[:-1]

        else:
            if self._count_args == 1:
                return range(self.first_args)
            elif self._count_args == 2:
                return range(self.first_args, self.second_arg)
            elif self._count_args == 3:
                return range(self.first_args, self.second_arg, self.third_arg)

    def _is_float_function(self):
        for arg in self.args_list:
            if isinstance(arg, float):
                return True
        return False

    def _count_elements(self):
        count = 0
        for i in self.args_list:
            if i != None:
                count += 1
        return count


if __name__ == '__main__':

    assert (list(frange(5)) == [0, 1, 2, 3, 4])
    assert (list(frange(2, 5)) == [2, 3, 4])
    assert (list(frange(2, 10, 2)) == [2, 4, 6, 8])
    assert (list(frange(10, 2, -2)) == [10, 8, 6, 4])
    assert (list(frange(1, 5)) == [1, 2, 3, 4])
    assert (list(frange(0, 5)) == [0, 1, 2, 3, 4])
    assert (list(frange(0, 0)) == [])
    assert (list(frange(100, 0)) == [])
    assert (list(frange(2, 5.5, 1.5)) == [2, 3.5, 5])
