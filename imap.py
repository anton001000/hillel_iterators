

class imap:
    def __init__(self, function, *args):
        self.function = function
        self.args = args
        self._list_results = self._execute_imap()
        self._pointer = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self._pointer == len(self._list_results):
            raise StopIteration
        result = self._list_results[self._pointer]
        self._pointer += 1
        return result

    def _execute_imap(self):
        res = []
        for arg in self.args:
            if isinstance(arg, list):
                for i in arg:
                    res.append(self.function(i))
            else:
                res.append(self.function(arg))
        return res


if __name__ == '__main__':
    def two(arg):
        return arg ** 2

    list_of_words = ['one', 'two', 'list', '', 'dict']

    m = list(imap(str.upper, list_of_words))
    l = list(imap(str.upper, "34545", "wedf", "wefwef"))
    x = list(imap(two, 10, 50, 100))


    print(m)
    print(l)
    print(x)
